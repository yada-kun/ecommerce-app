import { Box, createTheme } from '@mui/material';

const appTheme = createTheme({
  palette: {
    common: {
      black: '#111',
      white: '#f1f1f1',
    },
    primary: {
      main: '#AD0000',
      secondary: '#1C2536',
    },
  },
  components: {
    MuiTableCell: {
      styleOverrides: {
        root: {
          '&.MuiTableCell-head': { outline: 'none' },
          border: '0px',
          outline: 'none',
        },
      },
    },
    MuiTableHead: {
      styleOverrides: {
        root: {
          backgroundColor: 'rgba(255,255,255,0.4)',
          height: '10px',
        },
      },
    },
    MuiTablePagination: {
      defaultProps: {
        component: Box,
        showFirstButton: true,
        showLastButton: true,
        rowsPerPageOptions: [5, 10, 25, 50, 100],
      },
      styleOverrides: {
        root: ({ theme }) => ({
          minHeight: '53px',
          border: 'none',
          '& .MuiIconButton-root:hover': {
            color: theme.palette.primary.main,
          },
        }),
        menuItem: {
          height: 'max-content',
          padding: '4px 0',
          alignItems: 'center',
          justifyContent: 'center',
        },
      },
    },

    MuiTextField: {
      styleOverrides: {
        root: {
          fontSize: '1rem',
        },
      },
    },
    MuiButton: {
      defaultProps: {
        variant: 'contained',
        disableElevation: true,
      },
      styleOverrides: {
        root: {
          height: 48,
          textTransform: 'capitalize',
        },
      },
    },

    MuiFilledInput: {
      defaultProps: {
        disableUnderline: true,
      },
      styleOverrides: {
        root: ({ theme }) => ({
          borderRadius: 6,
          '&.Mui-error': {
            backgroundColor:
              theme.palette.mode === 'dark' ? '#ff6b6b22' : '#ffe3e3aa',
          },
        }),
      },
    },
  },
  shape: {
    borderRadius: 6,
  },
});

export default appTheme;
