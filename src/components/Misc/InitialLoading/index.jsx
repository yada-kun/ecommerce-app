import { Box } from '@mui/material';

import LoadingAnimation from './_LoadingAnimation';

const InitialLoading = () => {
  return (
    <Box
      width="100%"
      height="100vh"
      display="grid"
      sx={{
        placeItems: 'center',
      }}
    >
      <LoadingAnimation />
    </Box>
  );
};

export default InitialLoading;
