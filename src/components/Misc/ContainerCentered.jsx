import { Box } from '@mui/material';

import { styled } from '@mui/system';

const ContainerCentered = styled(Box)({
  height: '100vh',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  paddingInline: '1rem',
});

export default ContainerCentered;
