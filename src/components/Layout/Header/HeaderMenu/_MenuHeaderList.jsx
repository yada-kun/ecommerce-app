import { Box, Link } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';

import { MenuList } from './_MenuList';

const MenuHeaderList = () => {
  return (
    <Box
      gap={3}
      fontSize={20}
      marginLeft={-15}
      sx={{
        display: {
          xs: 'none',
          md: 'flex',
        },
      }}
    >
      {MenuList.map((list, i) => (
        <Link
          key={i}
          component={RouterLink}
          to={`/${list.toLowerCase()}`}
          sx={{
            textDecoration: 'none',
            color: '#333',
            '&:hover': {
              color: ({ palette }) => palette.primary.main,
            },
          }}
        >
          {list}
        </Link>
      ))}
    </Box>
  );
};

export default MenuHeaderList;
