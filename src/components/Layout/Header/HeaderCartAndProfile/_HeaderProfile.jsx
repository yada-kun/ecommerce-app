import { useState } from 'react';
import { Avatar, Box, Button } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';

import { useUser } from '@store/useAuth';

import ProfileMenu from './_ProfileMenu';

const stringAvatar = (fullName) => {
  return {
    children: fullName
      .split(' ')
      .map((el) => el[0].toUpperCase())
      .join(''),
  };
};

const HeaderProfile = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const user = useUser();
  let fullName;

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  if (user) {
    fullName = `${user.firstName} ${user.lastName}`;
  }
  return (
    <Box>
      {user ? (
        user?.image ? (
          <Avatar
            alt={`${user.firstName} ${user.lastName} `}
            src={user.image}
            onClick={handleClick}
            sx={{
              '&:hover': {
                cursor: 'pointer',
              },
            }}
          />
        ) : (
          <Avatar
            {...stringAvatar(fullName)}
            sx={{
              fontSize: '16px',
              '&:hover': {
                cursor: 'pointer',
              },
            }}
            onClick={handleClick}
          />
        )
      ) : (
        <Button
          component={RouterLink}
          to="/signin"
          sx={{
            textDecoration: 'none',
            fontSize: '1.1rem',
            width: '100px',
            height: '30px',
            color: ({ palette }) => palette.common.white,
            '&:hover': {
              opacity: 0.8,
            },
          }}
        >
          Login
        </Button>
      )}
      <ProfileMenu handleClose={handleClose} anchorEl={anchorEl} />
    </Box>
  );
};

export default HeaderProfile;
