import { Box, IconButton } from '@mui/material';
import LocalMallIcon from '@mui/icons-material/LocalMall';
import CartCounter from './_CartCounter';
import { Link } from 'react-router-dom';

const HeaderCart = () => {
  return (
    <Box position="relative">
      <CartCounter />
      <IconButton
        sx={{
          '&:hover': {
            color: ({ palette }) => palette.primary.main,
          },
        }}
        component={Link}
        to="/cart"
      >
        <LocalMallIcon fontSize="small" />
      </IconButton>
    </Box>
  );
};
export default HeaderCart;
