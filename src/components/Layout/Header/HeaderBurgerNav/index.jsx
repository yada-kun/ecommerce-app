import React from 'react';

import MenuIcon from '@mui/icons-material/Menu';
import { Box, IconButton } from '@mui/material';

const HeaderBurgerNav = ({ toggleDrawer }) => {
  return (
    <Box
      sx={{
        display: {
          xs: 'block',
          md: 'none',
        },
      }}
    ></Box>
  );
};

export default HeaderBurgerNav;
