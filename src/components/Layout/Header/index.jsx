// components
import HeaderWrapper from './_HeaderWrapper';
import HeaderLogo from './_HeaderLogo';
import MenuHeaderList from './HeaderMenu/_MenuHeaderList';
import HeaderCartAndProfile from './HeaderCartAndProfile/_HeaderCartAndProfile';

const Header = () => {
  return (
    <HeaderWrapper>
      <HeaderLogo />
      <MenuHeaderList />
      <HeaderCartAndProfile />
    </HeaderWrapper>
  );
};

export default Header;
