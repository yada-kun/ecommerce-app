import { Box } from '@mui/material';
import { Fragment } from 'react';
import { useLocation } from 'react-router-dom';
import Footer from './Footer';
import Header from './Header';
import Sidebar from './Sidebar';

const Layout = ({ children }) => {
  const location = useLocation();
  const isAdmin =
    location.pathname === '/admin' ||
    location.pathname === '/admin/dashboard' ||
    location.pathname === '/admin/user' ||
    location.pathname === '/admin/products' ||
    location.pathname === '/admin/orders';

  const isNotSigninSignup = !(
    location.pathname === '/signin' ||
    location.pathname === '/signup' ||
    location.pathname === '/' ||
    location.pathname === '/home'
  );

  if (isAdmin)
    return (
      <Fragment>
        <Box display="flex">
          <Sidebar />
          {children}
        </Box>
      </Fragment>
    );
  else
    return (
      <Fragment>
        {isNotSigninSignup && <Header />}
        {children}
        <Footer />
      </Fragment>
    );
};

export default Layout;
