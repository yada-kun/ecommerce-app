import DashboardIcon from '@mui/icons-material/Dashboard';
import PersonIcon from '@mui/icons-material/Person';
import ProductionQuantityLimitsIcon from '@mui/icons-material/ProductionQuantityLimits';
import SellIcon from '@mui/icons-material/Sell';

export const menulist = [
  { name: 'Dashboard', image: DashboardIcon },
  { name: 'User', image: PersonIcon },
  { name: 'Products', image: ProductionQuantityLimitsIcon },
  { name: 'Orders', image: SellIcon },
];
