import { LoadingButton } from '@mui/lab';

const SideBarSigninSignout = ({ isLoading, logoutHandler, children }) => {
  return (
    <LoadingButton
      variant="text"
      loading={isLoading}
      onClick={logoutHandler}
      sx={{
        height: '30px',
        color: ({ palette }) => palette.common.white,
        '&:hover': {
          color: ({ palette }) => palette.primary.main,
        },
        marginBottom: '10px',
      }}
    >
      {children}
    </LoadingButton>
  );
};

export default SideBarSigninSignout;
