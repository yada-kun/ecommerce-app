import { Box } from '@mui/material';

const FormContainer = (props) => {
  return (
    <Box
      component="form"
      width="100%"
      maxWidth={550}
      padding={3.5}
      display="flex"
      flexDirection="column"
      gap={3.4}
      alignItems="center"
      borderRadius={2}
      {...props}
      sx={{
        backgroundColor: ({ palette }) => palette.background.paper,
        border: ({ palette }) => `1px solid ${palette.divider}`,
        ...props.sx,
      }}
    >
      {props.children}
    </Box>
  );
};

export default FormContainer;
