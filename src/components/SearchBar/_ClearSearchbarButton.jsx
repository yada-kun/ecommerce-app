import { Fade } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import ClearIcon from '@mui/icons-material/Clear';

const ClearSearchbarButton = (props) => {
  return (
    <Fade in={props.show} timeout={200}>
      <IconButton onClick={props.onClear} size="small">
        <ClearIcon />
      </IconButton>
    </Fade>
  );
};

export default ClearSearchbarButton;
