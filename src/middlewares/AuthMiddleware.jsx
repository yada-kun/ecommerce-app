import { Fragment } from 'react';

import InitialLoading from '@components/Misc/InitialLoading';
import { useRefreshAccessToken } from '@hooks/authentication/useRefreshToken';

const AuthMiddleware = ({ children }) => {
  const { isLoading } = useRefreshAccessToken();

  if (isLoading) return <InitialLoading />;

  return <Fragment>{children}</Fragment>;
};

export default AuthMiddleware;
