import { useEffect, useState } from 'react';

import { Alert } from '@mui/material';

const WrongEmailPasswordAlert = ({ error }) => {
  const [alert, setAlert] = useState(false);
  let alertTimer;

  const handleResetAlert = () => {
    setAlert(false);
    clearTimeout(alertTimer);
  };

  const handleSetAlert = () => {
    setAlert(true);

    alertTimer = setTimeout(() => {
      setAlert(false);
    }, 6_000);
  };

  useEffect(() => {
    if (error) {
      handleSetAlert();
    }
  }, [error]);

  useEffect(() => {
    return () => {
      clearTimeout(alertTimer);
    };
  }, []);

  if (alert)
    return (
      <Alert
        severity="error"
        onClose={handleResetAlert}
        sx={{
          width: '100%',
        }}
      >
        {error.response.status === 401
          ? ' Invalid Email or Password.'
          : error.response.status === 500
          ? 'Internal Server error'
          : error.message}
      </Alert>
    );
};

export default WrongEmailPasswordAlert;
