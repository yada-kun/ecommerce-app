import { useEffect, useState } from 'react';

import Alert from '@mui/material/Alert';
import { useSearchParams } from 'react-router-dom';

const RegisteredSuccessfully = () => {
  const [searchParams, setSearchParams] = useSearchParams();

  const [openAlert, setOpenAlert] = useState(searchParams.has('register'));

  let alertTimer;

  useEffect(() => {
    if (openAlert) {
      alertTimer = setTimeout(handleResetSearchParams, 8_000);
    }

    return () => {
      clearTimeout(alertTimer);
    };
  }, []);

  const handleCloseAlert = () => {
    handleResetSearchParams();
    clearTimeout(alertTimer);
  };

  const handleResetSearchParams = () => {
    setOpenAlert(false);
    searchParams.delete('register');
    setSearchParams(searchParams);
  };

  return openAlert ? (
    <Alert onClose={handleCloseAlert}>
      You have been registered successfully. Please sign in to continue.
    </Alert>
  ) : null;
};

export default RegisteredSuccessfully;
