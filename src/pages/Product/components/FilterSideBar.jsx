import { Typography, Link, Box } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';

const FilterSideBar = () => {
  return (
    <Box
      width="250px"
      border="1px solid lightgray"
      paddingY="20px"
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      gap="15px"
      position="sticky"
      top={20}
    >
      <Typography variant="p" fontSize="1.2rem">
        Categories
      </Typography>
      <Box display="flex" flexDirection="column" gap={1}>
        <Link
          component={RouterLink}
          sx={{
            textDecoration: 'none',
            color: ({ palette }) => palette.common.black,
          }}
        >
          Shirt
        </Link>
        <Link
          component={RouterLink}
          sx={{
            textDecoration: 'none',
            color: ({ palette }) => palette.common.black,
          }}
        >
          Polo
        </Link>
        <Link
          component={RouterLink}
          sx={{
            textDecoration: 'none',
            color: ({ palette }) => palette.common.black,
          }}
        >
          Long Sleeve
        </Link>
        <Link
          component={RouterLink}
          sx={{
            textDecoration: 'none',
            color: ({ palette }) => palette.common.black,
          }}
        >
          Bow Tie
        </Link>
      </Box>
    </Box>
  );
};

export default FilterSideBar;
