import FilterSideBar from './components/FilterSideBar';
import ProductContent from './components/ProductContent';
import ProductPageWrapper from './components/ProductPageWrapper';

const ProductPage = () => {
  return (
    <ProductPageWrapper>
      <FilterSideBar />
      <ProductContent />
    </ProductPageWrapper>
  );
};

export default ProductPage;
