import { Box, Skeleton, Stack } from '@mui/material';
import React from 'react';

const ProductDetailSkeletonLoader = () => {
  return (
    <Box width="1024px" display="flex" marginX="auto" gap={10}>
      <Box>
        <Skeleton variant="rectangular" width={320} height={250} />
      </Box>
      <Box flex={3}>
        <Skeleton variant="text" />
        <Skeleton variant="text" />
        <Skeleton variant="text" />
        <Skeleton variant="text" />
        <Skeleton variant="text" />

        <Stack paddingTop={3} spacing={5}>
          <Skeleton width={100} height={30} variant="rectangular" />
          <Box display="flex" gap={2}>
            <Skeleton width={100} height={50} variant="rectangular" />
            <Skeleton width={100} height={50} variant="rectangular" />
          </Box>
        </Stack>
      </Box>
    </Box>
  );
};

export default ProductDetailSkeletonLoader;
