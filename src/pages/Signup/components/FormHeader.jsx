import { Link, Stack, Typography } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';

export const FormHeader = () => {
  return (
    <Stack width="100%" spacing={2}>
      <Typography fontSize={26} fontWeight={600}>
        Sign Up
      </Typography>

      <Stack direction="row" spacing={0.5}>
        <Typography fontSize={18}>Already have an account?</Typography>

        <Link
          component={RouterLink}
          to="/signin"
          underline="hover"
          fontSize={18}
          fontWeight={600}
        >
          Sign in
        </Link>
      </Stack>
    </Stack>
  );
};
