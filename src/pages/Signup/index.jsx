// library
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import useSignup from './hooks/useSignup';

// components
import ControlledTextField from '@components/ControlledInputs/ControlledTextField';
import FormContainer from '@components/Form/FormContainer';
import ContainerCentered from '@components/Misc/ContainerCentered';
import signUpFormSchema from './schema/signUpFormSchema';
import ControlledPasswordTextField from '@components/ControlledInputs/ControlledPasswordTextField';

import { SubmitButton } from '@components/Form/SubmitButton';
import { FormHeader } from './components/FormHeader';
import BackToHome from '@pages/Signin/components/BackToHome';
import RegisterErrorAlert from './components/RegisterErrorAlert';

const Signup = () => {
  const { mutate: submitForm, isLoading, error } = useSignup();
  const { control, handleSubmit } = useForm({
    defaultValues: {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
    },
    mode: 'onSubmit',
    resolver: zodResolver(signUpFormSchema),
  });
  return (
    <ContainerCentered>
      <BackToHome />
      <FormContainer onSubmit={handleSubmit((data) => submitForm(data))}>
        <FormHeader />
        <RegisterErrorAlert error={error} />
        <ControlledTextField
          size="small"
          control={control}
          name="firstName"
          label="First Name"
          variant="filled"
          required
        />

        <ControlledTextField
          size="small"
          control={control}
          name="lastName"
          label="Last Name"
          variant="filled"
          required
        />

        <ControlledTextField
          size="small"
          control={control}
          name="email"
          label="Email Address"
          variant="filled"
          required
        />

        <ControlledPasswordTextField
          size="small"
          control={control}
          name="password"
          label="Password"
          variant="filled"
          required
        />

        <SubmitButton isLoading={isLoading}>Register</SubmitButton>
      </FormContainer>
    </ContainerCentered>
  );
};

export default Signup;
