import axios from 'axios';

const RegisterUser = async (data) => {
  const {
    data: { data: user },
  } = await axios.post(
    'https://capstone2-anuncio.onrender.com/api/v1/users/signup',
    data,
  );

  return user;
};

export default RegisterUser;
