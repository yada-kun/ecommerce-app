import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import ControlledTextField from '@components/ControlledInputs/ControlledTextField';
import useAddProduct from '../hooks/useAddProduct';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { LoadingButton } from '@mui/lab';
import FormContainer from '@components/Form/FormContainer';
import { addProductSchema } from '../schema/AddProductSchema';
import Dropzone from './Dropzone';

export default function FormDialog({ open, handleClickOpen, handleClose }) {
  const [droppedImage, setDroppedImage] = React.useState(null);
  const [rawImage, setRawImage] = React.useState(null);

  const onDrop = React.useCallback((acceptedFiles) => {
    const file = acceptedFiles[0];
    setRawImage(file);
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      setDroppedImage(reader.result);
    };
  }, []);

  const { addProductText, addProductImage } = useAddProduct();
  const { control, handleSubmit, reset } = useForm({
    defaultValues: {
      title: '',
      category: '',
      price: +'',
      description: '',
      stock: +'',
    },
    mode: 'onSubmit',
    resolver: zodResolver(addProductSchema),
  });

  const handleAddProduct = (e) => {
    handleSubmit((data) => {
      addProductText.mutate(data, {
        onSuccess: (data) => {
          data.image = rawImage;
          addProductImage.mutate(data, {
            onSuccess: handleResetForm,
          });
        },
      });
    })(e.preventDefault());
  };

  const handleResetForm = () => {
    reset({
      title: '',
      category: '',
      price: '',
      description: '',
      stock: '',
    });
    // Close the dialog
    setRawImage('');
    setDroppedImage('');
    handleClose();
  };

  return (
    <Dialog open={open} onClose={handleClose} fullWidth>
      <DialogTitle>Add a product</DialogTitle>
      <DialogContent>
        <FormContainer onSubmit={handleAddProduct}>
          <ControlledTextField
            autoFocus
            control={control}
            name="title"
            label="Product Title"
            variant="filled"
            required
          />
          <ControlledTextField
            autoFocus
            control={control}
            name="category"
            label="Category "
            variant="filled"
            required
          />

          <Dropzone droppedImage={droppedImage} onDrop={onDrop} />

          <ControlledTextField
            label="price"
            control={control}
            name="price"
            rules={{ min: 1 }}
            id="formatted-numberformat-input"
            variant="filled"
            fullWidth
            required
          />

          <ControlledTextField
            control={control}
            name="description"
            label="description"
            variant="filled"
            required
          />
          <ControlledTextField
            control={control}
            name="stock"
            type="number"
            label="stock"
            variant="filled"
            required
          />

          <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <LoadingButton
              type="submit"
              variant="contained"
              loading={addProductText.isLoading || addProductImage.isLoading}
            >
              Add Product
            </LoadingButton>
          </DialogActions>
        </FormContainer>
      </DialogContent>
    </Dialog>
  );
}
