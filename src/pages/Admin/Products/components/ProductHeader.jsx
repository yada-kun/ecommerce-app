import { Box, Button, Typography } from '@mui/material';
import { Fragment, useState } from 'react';
import FormDialog from './AddProductForm';

const ProductHeader = () => {
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Fragment>
      <Box
        width="100%"
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        paddingX="30px"
      >
        <Typography variant="h4">Products</Typography>

        <Button
          sx={{ background: '#1C2536', height: '35px' }}
          onClick={handleClickOpen}
        >
          +Add Product
        </Button>
      </Box>
      <FormDialog
        open={open}
        handleClickOpen={handleClickOpen}
        handleClose={handleClose}
      />
    </Fragment>
  );
};

export default ProductHeader;
