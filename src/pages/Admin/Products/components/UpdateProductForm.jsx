import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import ControlledTextField from '@components/ControlledInputs/ControlledTextField';

import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { LoadingButton } from '@mui/lab';
import FormContainer from '@components/Form/FormContainer';
import { addProductSchema } from '../schema/AddProductSchema';
import Dropzone from './Dropzone';
import useUpdateProduct from '../hooks/useUpdateProduct';

export default function UpdateProductForm({
  open,
  handleClickOpen,
  handleClose,
  item,
  setItem,
}) {
  const [droppedImage, setDroppedImage] = React.useState(null);
  const [rawImage, setRawImage] = React.useState(null);

  const onDrop = React.useCallback((acceptedFiles) => {
    const file = acceptedFiles[0];
    setRawImage(file);
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      setDroppedImage(reader.result);
    };
  }, []);

  const { addProductText, addProductImage } = useUpdateProduct();
  const { control, handleSubmit, reset } = useForm({
    defaultValues: {
      title: item?.title,
      category: item?.category,
      price: item?.price,
      description: item?.description,
      stock: item?.stock,
    },
    mode: 'onSubmit',
    resolver: zodResolver(addProductSchema),
  });

  const handleAddProduct = (e) => {
    handleSubmit((data) => {
      data._id = item._id;
      addProductText.mutate(data, {
        onSuccess: (data) => {
          data.image = rawImage;
          addProductImage.mutate(data, {
            onSuccess: handleResetForm,
          });
        },
      });
    })(e.preventDefault());
  };

  const handleResetForm = () => {
    reset({
      title: '',
      category: '',
      price: '',
      description: '',
      stock: '',
    });
    // Close the dialog
    setItem('');
    setRawImage('');
    setDroppedImage('');
    handleClose();
  };

  return (
    <Dialog open={open} onClose={handleClose} fullWidth>
      <DialogTitle>Update a product</DialogTitle>
      <DialogContent>
        <FormContainer onSubmit={handleAddProduct}>
          <ControlledTextField
            autoFocus
            control={control}
            name="title"
            label="Product Title"
            variant="filled"
            required
          />
          <ControlledTextField
            autoFocus
            control={control}
            name="category"
            label="Category "
            variant="filled"
            required
          />

          <Dropzone droppedImage={droppedImage} onDrop={onDrop} />

          <ControlledTextField
            label="price"
            control={control}
            name="price"
            rules={{ min: 1 }}
            id="formatted-numberformat-input"
            variant="filled"
            fullWidth
            required
          />

          <ControlledTextField
            control={control}
            name="description"
            label="description"
            variant="filled"
            required
          />
          <ControlledTextField
            control={control}
            name="stock"
            type="number"
            label="stock"
            variant="filled"
            required
          />

          <DialogActions>
            <Button onClick={handleResetForm}>Cancel</Button>
            <LoadingButton
              type="submit"
              variant="contained"
              loading={addProductText.isLoading || addProductImage.isLoading}
            >
              Update Product
            </LoadingButton>
          </DialogActions>
        </FormContainer>
      </DialogContent>
    </Dialog>
  );
}
