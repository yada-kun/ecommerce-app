import { useContext, useState } from 'react';

import useArchiveProduct from '../hooks/useArchiveProduct';
import UpdateProductForm from './UpdateProductForm';

import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { Box, Rating, TablePagination } from '@mui/material';
import ArchiveIcon from '@mui/icons-material/Archive';
import AssignmentTurnedInIcon from '@mui/icons-material/AssignmentTurnedIn';

import { ProductsContext } from '../context/ProductsContext';

import EditIcon from '@mui/icons-material/Edit';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: 'rgba(184,190,200, 0.2)',
    color: theme.palette.common.black,
    padding: '10px',
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

export default function AdminProductTable() {
  const [open, setOpen] = useState(false);
  const [singleProduct, setSingleProduct] = useState(null);

  const {
    productsQuery: { products, isLoading },
    changePage,
    changePageSize,
    pageSize,
    count,
    currentPage,
  } = useContext(ProductsContext);

  const { mutate: archiveProduct, isLoading: isArchiveLoading } =
    useArchiveProduct();

  const handleClickOpen = (item) => {
    setSingleProduct(item);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  if (isLoading || isArchiveLoading) {
    return <p>Loading...</p>;
  }
  const handleArchiveProduct = (item) => {
    if (item.isActive === true) item.isActive = false;
    else item.isActive = true;

    console.log(item);
    archiveProduct(item);
  };

  return (
    <Box
      marginX="30px"
      maxHeight="60vh"
      display="flex"
      flexDirection="column"
      overflow="auto"
    >
      <TableContainer>
        <Table
          aria-label="customized table"
          //
        >
          <TableHead>
            <TableRow>
              <StyledTableCell>Edit</StyledTableCell>
              <StyledTableCell>Image</StyledTableCell>
              <StyledTableCell>Title</StyledTableCell>
              <StyledTableCell>Description</StyledTableCell>
              <StyledTableCell>Category</StyledTableCell>
              <StyledTableCell>Price</StyledTableCell>
              <StyledTableCell>Active</StyledTableCell>
              <StyledTableCell>Rating</StyledTableCell>
              <StyledTableCell>Action</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {products.map((item) => (
              <TableRow key={item.title}>
                <StyledTableCell>
                  <EditIcon
                    onClick={handleClickOpen.bind(null, item)}
                    sx={{
                      cursor: 'pointer',
                      '&:hover': {
                        color: ({ palette }) => palette.primary.main,
                      },
                    }}
                  />
                </StyledTableCell>
                <StyledTableCell width="80px">
                  <img width="80px" src={item.image} alt={item.name} />
                </StyledTableCell>

                <StyledTableCell>{item.title}</StyledTableCell>

                <StyledTableCell>{item.description}</StyledTableCell>
                <StyledTableCell>{item.category}</StyledTableCell>
                <StyledTableCell>{item.price}</StyledTableCell>
                <StyledTableCell>
                  {item.isActive ? (
                    <p
                      style={{
                        backgroundColor: 'lightgreen',
                        paddingInline: '10px',
                        borderRadius: '10px',
                      }}
                    >
                      Active
                    </p>
                  ) : (
                    <p
                      style={{
                        backgroundColor: '#ff8383	',
                        paddingInline: '10px',
                        borderRadius: '10px',
                      }}
                    >
                      Inactive
                    </p>
                  )}
                </StyledTableCell>
                <StyledTableCell>
                  <Rating name="read-only" value={item.rating} readOnly />
                </StyledTableCell>
                <StyledTableCell>
                  {item.isActive ? (
                    <ArchiveIcon
                      onClick={handleArchiveProduct.bind(null, item)}
                      sx={{
                        cursor: 'pointer',
                        '&:hover': {
                          color: ({ palette }) => palette.primary.main,
                        },
                      }}
                    />
                  ) : (
                    <AssignmentTurnedInIcon
                      onClick={handleArchiveProduct.bind(null, item)}
                      sx={{
                        cursor: 'pointer',
                        '&:hover': {
                          color: ({ palette }) => palette.primary.main,
                        },
                      }}
                    />
                  )}
                </StyledTableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={count}
        rowsPerPage={pageSize}
        page={currentPage}
        onPageChange={changePage}
        onRowsPerPageChange={changePageSize}
      />
      {singleProduct && (
        <UpdateProductForm
          item={singleProduct}
          setItem={setSingleProduct}
          open={open}
          handleClickOpen={handleClickOpen}
          handleClose={handleClose}
        />
      )}
    </Box>
  );
}
