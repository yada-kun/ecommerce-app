import { useMutation, useQueryClient } from '@tanstack/react-query';
import { addProductImageApi } from '../api/addProductImage';
import { toast } from 'react-toastify';
import { archiveProduct } from '../api/arhciveProductApi';

const useUpdateProduct = () => {
  const queryClient = useQueryClient();
  const addProductText = useMutation({
    mutationFn: archiveProduct,
    onSuccess: (data) => {},
    onError: (error) => {
      toast.error(error);
    },
    retry: 0,
  });

  const addProductImage = useMutation({
    mutationFn: (data) => {
      console.log(data);
      return addProductImageApi(data);
    },
    onSuccess: () => {
      queryClient.invalidateQueries(['product']);
      toast.success('Product successfully updated');
    },
  });

  return { addProductText, addProductImage };
};

export default useUpdateProduct;
