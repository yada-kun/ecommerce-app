import { axiosClient } from '@config/axios';

export const archiveProduct = async (data) => {
  data.price = +data.price;
  data.stock = +data.stock;
  const {
    data: { data: product },
  } = await axiosClient.patch(`/products/${data._id}`, data);

  return product;
};
