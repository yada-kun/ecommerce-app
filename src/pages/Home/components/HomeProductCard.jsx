import ProductCard from '@components/ProductCard';
import ProductSkeletonLoader from '@components/ProductCard/components/ProductSkeletonLoader';
import useGetProducts from '@hooks/products/useGetProduct';

import { Box, Typography } from '@mui/material';
import useGetCart from '@pages/Cart/hooks/useGetCart';
import { useUser } from '@store/useAuth';
import { generateSkeletonLoader } from '@utils/generateSkeletonLoader';

const HomeProductCard = () => {
  const user = useUser();

  const { products, isLoading, isError } = useGetProducts(1, 8);
  const hasUser = !!user?.email && user?.isAdmin === false;
  const {
    data,
    isLoading: isLoadingCart,
    isError: isCartError,
    refetch,
  } = useGetCart(hasUser, 1, 10);

  const isMappable = !isLoading && !isLoadingCart && !isError && !isCartError;

  return (
    <Box
      display="flex"
      maxWidth="1220px"
      flexWrap="wrap"
      justifyContent="center"
      alignItems="center"
      flexDirection="column"
      marginX="auto"
      marginY="120px"
      gap={5}
    >
      <Typography variant="h4"> Products</Typography>
      <Box
        display="flex"
        flexWrap="wrap"
        justifyContent="center"
        alignItems="center"
        gap={5}
      >
        {hasUser
          ? isMappable
            ? products.map((product, i) => {
                const isInCart = data?.itemIds?.includes(product._id);
                return (
                  <ProductCard
                    key={i}
                    item={product}
                    isInCart={isInCart}
                    refetch={refetch}
                  />
                );
              })
            : generateSkeletonLoader(8, <ProductSkeletonLoader />)
          : isLoading
          ? generateSkeletonLoader(8, <ProductSkeletonLoader />)
          : products.map((product, i) => {
              return <ProductCard key={i} item={product} isInCart={false} />;
            })}
      </Box>
    </Box>
  );
};

export default HomeProductCard;
