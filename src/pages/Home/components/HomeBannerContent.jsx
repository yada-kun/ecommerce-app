import { Box, Button, Typography } from '@mui/material';
import React from 'react';

const HomeBannerContent = () => {
  return (
    <Box
      paddingY="200px"
      paddingX="50px"
      sx={{
        display: 'flex',
        justifyContent: {
          xs: 'center',
        },
      }}
    >
      <Box
        flex={1}
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: {
            xs: 'center',
            lg: 'flex-start',
          },
          justifyContent: {
            xs: 'center',
          },
          paddingLeft: {
            lg: '200px',
          },

          gap: '20px',
        }}
      >
        <Typography
          variant="h2"
          sx={{
            fontSize: 'clamp(2rem, 10vw, 6rem)',
            letterSpacing: {
              md: '5px',
            },
            fontWeight: {
              md: 600,
            },
          }}
        >
          Big Sale{' '}
          <span
            style={{
              fontSize: 'clamp(2rem, 10vw, 6rem)',
              color: '#AD0000',
              letterSpacing: '5px',
            }}
          >
            Today!
          </span>
        </Typography>
        <Typography
          sx={{
            display: {
              xs: 'none',
              sm: 'block',
            },
            letterSpacing: '1px',
            paddingLeft: '15px',
          }}
        >
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit
          tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
        </Typography>
        <Button sx={{ width: '200px' }}>Shop Now</Button>
      </Box>
      <Box
        flex={1}
        sx={{
          display: {
            xs: 'none',
            lg: 'block',
          },
        }}
      ></Box>
    </Box>
  );
};

export default HomeBannerContent;
