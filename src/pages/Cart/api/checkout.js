import { axiosClient } from '@config/axios';

export const PaymentCheckout = async (data) => {
  const response = await axiosClient.post(
    '/stripe/create-checkout-session',
    data,
  );

  return response;
};
