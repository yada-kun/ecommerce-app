import { useCallback } from 'react';

import useDeleteCart from './useDeleteCart';
// import useUpdateCartQuantity from './useUpdateCartQuantity';

const useCartFunction = () => {
  const { mutate: deleteProduct } = useDeleteCart();
  // const { mutate: updateCart } = useUpdateCartQuantity();

  const deleteItem = useCallback(
    (id) => {
      deleteProduct(id);
    },
    [deleteProduct],
  );

  const subtractQuantityHandler = useCallback(
    (i, quantity, setItemQuantity) => {
      const checkQuantity = +(quantity - 1);

      if (checkQuantity >= 1) {
        console.log(checkQuantity);
        setItemQuantity((prev) => {
          const updatedArray = [...prev];
          updatedArray[i] = quantity - 1;

          return updatedArray;
        });
      } else {
        setItemQuantity((prev) => {
          const updatedArray = [...prev];
          updatedArray[i] = 0;

          return updatedArray;
        });
      }
    },
    [],
  );

  const addQuantityHandler = (
    i,
    quantity,
    setItemQuantity,
    debouncedValue,
    item,
    itemQuantity,
    specific,
    setSpecific,
  ) => {
    setItemQuantity((prev) => {
      const updatedArray = [...prev];
      updatedArray[i] = quantity + 1;
      // value = updatedArray[i];
      return updatedArray;
    });
    console.log(debouncedValue);

    if (debouncedValue !== 0) {
      console.log('hello');
      // const updatedItem = {
      //   id: item.productId,
      //   quantity: debouncedValue,
      // };
      // updateCart(updatedItem);
    }
  };

  return { deleteItem, subtractQuantityHandler, addQuantityHandler };
};

export default useCartFunction;
