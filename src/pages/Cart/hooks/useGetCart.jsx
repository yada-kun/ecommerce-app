import { useCallback, useMemo } from 'react';

import { useQuery } from '@tanstack/react-query';
import { getProductsInCart } from '../api/getProductsInCart';

const useGetCart = (isUserAvailable, page, limit) => {
  const handleAddId = useCallback((data, quantity) => {
    const dataCopy = structuredClone(data);

    dataCopy.itemIds = dataCopy.docs[0]?.cart[0]?.items.map(
      (item) => item.productId,
    );

    return dataCopy;
  }, []);

  const getCartQuery = useQuery({
    queryKey: ['cart', page, limit],
    queryFn: () => getProductsInCart({ page, limit, quantity }),
    enabled: isUserAvailable,
    select: handleAddId,
  });

  const products = getCartQuery.data?.docs[0]?.cart[0]?.items
    ? getCartQuery.data.docs[0]
    : [];

  // const products = () => {
  //   if (getCartQuery.data?.docs[0]?.cart[0]?.items) {
  //     return getCartQuery.data.docs[0];
  //   }

  //   return [];
  // };

  const total = useMemo(() => {
    let sum = 0;

    if (products.length !== 0) {
      products.cart[0].items.forEach((item) => {
        sum += item.subTotal;
      });
    }

    return sum;
  }, [products]);

  const quantity = useMemo(() => {
    const qty = [];

    if (products.length !== 0) {
      products.cart[0].items.forEach((item) => {
        qty.push(item.quantity);
      });
    }

    return qty;
  }, [products]);

  return { ...getCartQuery, products, total, quantity };
};

export default useGetCart;
