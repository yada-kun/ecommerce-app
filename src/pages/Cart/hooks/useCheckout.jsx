import { useMutation } from '@tanstack/react-query';
import { PaymentCheckout } from '../api/checkout';

const useCheckout = () => {
  const checkout = useMutation({
    mutationFn: PaymentCheckout,
    retry: 0,
    onSuccess: (data) => {
      if (data.data.url) {
        window.location.href = data.data.url;
      }
    },
  });

  return checkout;
};

export default useCheckout;
