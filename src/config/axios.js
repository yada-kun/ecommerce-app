import axios from 'axios';

import { getUser, updateUser } from '@store/useAuth';
import { getRefreshedAccessToken } from '../api/authentication';
import { urlRefreshWhitelist } from './urlRefreshWhiteList';

const BASE_URL = 'https://capstone2-anuncio.onrender.com/api/v1';

export const axiosClient = axios.create({
  baseURL: BASE_URL,
  withCredentials: true,
  retry: 3, // Maximum number of retries
  retryDelay: 1000, // Delay between retries in milliseconds
});

const isAccessTokenExpiredError = (error) => {
  return (
    error.response &&
    error.response.status === 401 &&
    error.response.data.message === 'Access token expired'
  );
};

const isNotWhitelistedUrl = (originalRequest) => {
  return urlRefreshWhitelist.every(
    (whitelistedUrl) => originalRequest.url !== whitelistedUrl,
  );
};

axiosClient.interceptors.request.use(
  (config) => {
    const user = getUser();
    if (user?.accessToken) {
      config.headers.Authorization = `Bearer ${user.accessToken}`;
    }

    return config;
  },
  (error) => {
    return Promise.reject(error);
  },
);

axiosClient.interceptors.response.use(
  (response) => response,
  async (error) => {
    const originalRequest = error.config;
    const user = getUser();

    // Check if the error is due to an expired access token and the URL is not whitelisted
    if (
      isAccessTokenExpiredError(error) &&
      isNotWhitelistedUrl(originalRequest)
    ) {
      // Check if the request has already been retried
      if (!originalRequest.retryCount) {
        // Get a refreshed access token and update the user
        const refreshedUser = await getRefreshedAccessToken(user.refreshToken);
        updateUser(refreshedUser);
        // Retry the original request with the new access token
        originalRequest.retryCount = 1;
        originalRequest.headers.Authorization = `Bearer ${refreshedUser.accessToken}`;
        return axiosClient(originalRequest);
      } else {
        // Reject the request if the maximum number of retries has been exceeded
        return Promise.reject(error);
      }
    }

    return Promise.reject(error);
  },
);
