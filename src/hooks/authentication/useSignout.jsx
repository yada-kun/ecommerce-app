import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';

import { signout } from '@api/authentication';
import { useClearUser } from '@store/useAuth';

const useSignout = () => {
  const queryClient = useQueryClient();

  const navigate = useNavigate();
  const clearUser = useClearUser();

  const status = useMutation({
    mutationFn: signout,
    onSuccess: () => {
      clearUser();
      queryClient.removeQueries({ queryKey: ['refresh-access-token'] });
      navigate('/signin', {
        replace: true,
      });
    },
    onError: (error) => {
      if (error.response?.status === 403) {
        clearUser();
        navigate('/signin', {
          replace: true,
        });
      }
    },
  });

  return status;
};

export default useSignout;
