import { prefetchCartItems } from '@/api/prefetchCartItemsApi';
import { useQueryClient } from '@tanstack/react-query';

const usePrefetchCartData = async () => {
  const queryClient = useQueryClient();

  await queryClient.prefetchQuery({
    queryKey: ['cartItems'],
    queryFn: prefetchCartItems,
  });
};

export default usePrefetchCartData;
