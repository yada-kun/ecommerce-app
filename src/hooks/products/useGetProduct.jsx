import { useQuery } from '@tanstack/react-query';
import { getProductApi } from '@/api/getProductApi';

const useGetProducts = (page, limit) => {
  // get product data
  const productsQuery = useQuery({
    queryKey: ['product', page, limit],
    queryFn: () => getProductApi({ page, limit }),
  });

  const products = productsQuery.isLoading ? [] : productsQuery.data.data.docs;

  return { ...productsQuery, products };
};

export default useGetProducts;
