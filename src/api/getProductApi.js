import axios from 'axios';

export const getProductApi = async ({ page = 1, limit = 10 }) => {
  const { data } = await axios.get(
    `https://capstone2-anuncio.onrender.com/api/v1/products?page=${page}&limit=${limit}`,
  );

  return data;
};
