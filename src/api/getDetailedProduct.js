import axios from 'axios';

export const getDetailedProducts = async (id) => {
  const {
    data: { data: product },
  } = await axios.get(
    `https://capstone2-anuncio.onrender.com/api/v1/products/${id}`,
  );

  return product;
};
