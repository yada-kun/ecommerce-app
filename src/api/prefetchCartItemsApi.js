import { axiosClient } from '@config/axios';

export const prefetchCartItems = async () => {
  console.log(axiosClient);
  const {
    data: { data: cart },
  } = await axiosClient.get('/cart');

  return cart;
};
