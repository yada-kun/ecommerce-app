import NotFound from '@pages/NotFound';
// import Signin from '@pages/Signin';

import Signup from '@pages/Signup';

import Home from '@pages/Home';
import Signin from '@pages/Signin';

import Admin from '@pages/Admin';
import ProductAdminPage from '@pages/Admin/Products';
import ProductPage from '@pages/Product';
import ProductDetail from '@pages/Product/Detailed';
import Cart from '@pages/Cart';

const routes = [
  {
    path: '*',
    Component: NotFound,
    isPrivateRoute: false,
    adminRoute: false,
  },
  {
    path: '/',
    Component: Home,
    isPrivateRoute: false,
    adminRoute: false,
  },
  {
    path: '/home',
    Component: Home,
    isPrivateRoute: false,
    adminRoute: false,
  },
  {
    path: '/product',
    Component: ProductPage,
    isPrivateRoute: false,
    adminRoute: false,
  },
  {
    path: '/product/detail/:productId',
    Component: ProductDetail,
    isPrivateRoute: false,
    adminRoute: false,
  },
  // {
  //   path: '/profile',
  //   Component: Profile,
  //   isPrivateRoute: true,
  // },
  {
    path: '/signup',
    Component: Signup,
    isPrivateRoute: false,
    adminRoute: false,
  },
  {
    path: '/signin',
    Component: Signin,
    isPrivateRoute: false,
    adminRoute: false,
  },
  {
    path: '/cart',
    Component: Cart,
    isPrivateRoute: true,
    adminRoute: false,
  },
  {
    path: '/admin',
    Component: Admin,
    isPrivateRoute: false,
    adminRoute: true,
  },
  {
    path: '/admin/products',
    Component: ProductAdminPage,
    isPrivateRoute: false,
    adminRoute: true,
  },
];

export default routes;
