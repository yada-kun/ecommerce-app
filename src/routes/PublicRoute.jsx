import { Outlet, useNavigate } from 'react-router-dom';

import Layout from '@components/Layout';
import { useUser } from '@store/useAuth';
import { useEffect } from 'react';

const PublicRoute = () => {
  const user = useUser();
  const navigate = useNavigate();

  const isAuthenticated = !!user;

  useEffect(() => {
    if (isAuthenticated) return navigate('/', { replace: true });
  }, [isAuthenticated]);

  return (
    <Layout>
      <Outlet />
    </Layout>
  );
};

export default PublicRoute;
