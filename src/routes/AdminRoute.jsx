import Layout from '@components/Layout';
import { useUser } from '@store/useAuth';
import { Navigate, Outlet, useLocation } from 'react-router-dom';

const AdminRoute = () => {
  const user = useUser();
  const location = useLocation();

  console.log('hello');

  if (user?.isAdmin === true) {
    if (location.pathname !== '/cart')
      return <Layout isPrivateRoute={true}>{<Outlet />}</Layout>;
    else return <Navigate to="/" replace />;
  } else return <Navigate to="/" replace />;
};

export default AdminRoute;
