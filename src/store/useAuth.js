import { create } from 'zustand';

export const useAuth = create((Set) => ({
  user: null,
  setUser: (userData) =>
    Set(() => ({
      user: userData,
    })),
  clearUser: () =>
    Set(() => ({
      user: null,
    })),
}));

// export const useAuth = createStore((set) => ({
//   user: null,
//   setUser: (userData) => set({ user: userData }),
//   clearUser: () => set({ user: null }),
// }));

export const useUser = () => useAuth((state) => state.user);
export const useSetUser = () => useAuth((state) => state.setUser);
export const useClearUser = () => useAuth((state) => state.clearUser);
export const getUser = () => useAuth.getState().user;
export const updateUser = (userData) => {
  useAuth.setState(() => ({ user: userData }));
};
