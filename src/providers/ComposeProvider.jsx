import { Fragment } from 'react';

const ComposeProvider = (props) => {
  const { components = [], children } = props;

  return (
    <Fragment>
      {components.reduceRight((accumulator, Component) => {
        return <Component>{accumulator}</Component>;
      }, children)}
    </Fragment>
  );
};
export default ComposeProvider;
