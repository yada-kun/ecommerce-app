import { BrowserRouter } from 'react-router-dom';

import MuiThemeProvider from './MuiThemeProvider';
import TanstackQueryProvider from './TanstackQueryProvider';
import AuthMiddleware from '@middlewares/AuthMiddleware';

const rootProvider = [
  TanstackQueryProvider,
  BrowserRouter,
  MuiThemeProvider,
  AuthMiddleware,
];

export default rootProvider;
