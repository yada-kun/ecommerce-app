import ComposeProvider from '@providers/ComposeProvider';
import rootProvider from '@providers/RootProvider';
import Router from '@routes/Router';
import { ToastContainer } from 'react-toastify';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import 'react-toastify/dist/ReactToastify.css';

const App = () => {
  return (
    <ComposeProvider components={rootProvider}>
      <ToastContainer />
      <Router />
      <ReactQueryDevtools />
    </ComposeProvider>
  );
};

export default App;
